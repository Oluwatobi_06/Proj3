/*
 * File-related system call implementations.
 */

#include <types.h>
#include <kern/errno.h>
#include <kern/fcntl.h>
#include <kern/limits.h>
#include <kern/seek.h>
#include <kern/stat.h>
#include <lib.h>
#include <uio.h>
#include <proc.h>
#include <current.h>
#include <synch.h>
#include <copyinout.h>
#include <vfs.h>
#include <vnode.h>
#include <openfile.h>
#include <filetable.h>
#include <syscall.h>

/*
 * open() - get the path with copyinstr, then use openfile_open and
 * filetable_place to do the real work.
 */
int
sys_open(const_userptr_t upath, int flags, mode_t mode, int *retval)
{
	const int allflags = O_ACCMODE | O_CREAT | O_EXCL | O_TRUNC | O_APPEND | O_NOCTTY;

	char *kpath;
	struct openfile *file;
	struct filetable *ft; 
	int result = 0;
	bool append_flag= false;

	/* 
	 * Your implementation of system call open starts here.  
	 *
	 * Check the design document design/filesyscall.txt for the steps
	 */
	(void) upath; // suppress compilation warning until code gets written
	if(upath==NULL)
	{
		result= EFAULT;
		return result;
	}
	(void) flags; // suppress compilation warning until code gets written
	switch (flags){
		case O_RDONLY: break;
		case O_WRONLY: break;
		case O_RDWR: break;
		case O_RDONLY|O_CREAT: break;
		case O_WRONLY|O_CREAT: break;
		case O_WRONLY|O_APPEND:
							append_flag= true;
		 					break;
		case O_RDWR|O_CREAT: break;
		case O_RDWR|O_APPEND:
		 					append_flag=true;
		 					 break;
		case O_WRONLY|O_CREAT|O_TRUNC: break;
		case O_ACCMODE: break;

		default: result=  EINVAL;
				return result;
	}
	int slot= -1;
	for(int i=3; i< OPEN_MAX; i++)
	{
		if(curthread->ft_openfiles[i]==NULL)    //look for an empty slot
		{
				slot= i;
				break;
		}
	}
						//to check if there's no empty slot
	if (slot == -1)
	{
		result= EMFILE;     //too much open file in the system
		return result;
	}

	size_t actual;
	if((result= copyinstr(upath, kpath, PATH_MAX, &actual))!=0)
	{
		return result;
	}



	openfile_open(kpath,flags,mode,&file);

	if((result= filetable_place (ft,file, result ))!=0)
	{
		return result;
	}

	(void) mode; // suppress compilation warning until code gets written
	(void) retval; // suppress compilation warning until code gets written
	(void) allflags; // suppress compilation warning until code gets written
	(void) kpath; // suppress compilation warning until code gets written
	(void) file; // suppress compilation warning until code gets written

	return result;
}

/*
 * read() - read data from a file
 */
ssize_t
sys_read(int fd, userptr_t buf, size_t size )
{
       int result = 0;
       struct filetable *ft;
       struct openfile *file ;
       

       /* 
        * Your implementation of system call read starts here.  
        *
        * Check the design document design/filesyscall.txt for the steps
        */

        if((result = filetable_get(ft, fd, & file))!=0)
        {
        	return result;
        }

        lock_acquire(file->of_offsetlock);
        file = curthread-> ft_openfiles[fd];

        if(file ->of_accomde == O_WRONLY)
        {
        	return EBADF;
        }



        if( buf == NULL)
        {
        	return EFAULT;
        }


        struct uio user;
        struct iovec iov;

        iov.iov_base= buf;
        iov.iov_len= size;
        user.uio_iov= &iov;
        user.uio_iovcnt= 1;
        user.uio_offset= file->of_offset;
        user.uio_resid= size;
        user.uio_segflg= UIO_USERSPACE;
        user.uio_rw= UIO_READ;
        user.uio_space= curthread->p_addrspace;

        result= VOP_READ(file->of_vnode, & user);
        if(result)
        {
        	return result;
        }

       // *retval= size- user.uio_resid;
        //file-> of_offset= user.uio_offset;

        lock_release(file->of_offsetlock);


        



/*- translate the file descriptor number to an open file object
     (use filetable_get)
   - lock the seek position in the open file (but only for seekable objects)
   - check for files opened write-only
   - construct a struct: uio
   - call VOP_READ
   - update the seek position afterwards
   - unlock and filetable_put()
   - set the return value correctly
*/
       (void) fd; // suppress compilation warning until code gets written
       (void) buf; // suppress compilation warning until code gets written
       (void) size; // suppress compilation warning until code gets written
       (void) retval; // suppress compilation warning until code gets written

       return result;
}

/*
 * write() - write data to a file
 */
ssize_t sys_write(int fd,  userptr_t buf, size_t count)
{
       int result = 0;
       struct filetable *ft;
       struct openfile *file ;
       

       /* 
        * Your implementation of system call read starts here.  
        *
        * Check the design document design/filesyscall.txt for the steps
        */

        if((result = filetable_get(ft, fd, & file))!=0)
        {
        	return result;
        }

        lock_acquire(file->of_offsetlock);
        file = curthread-> ft_openfiles[fd];

        if(file ->of_accomde == O_RDONLY)
        {
        	return EBADF;
        }

        if( buf == NULL)
        {
        	return EFAULT;
        }

        struct uio user;
        struct iovec iov;

        iov.iov_base= buf;
        iov.iov_len= size;
        user.uio_iov= &iov;
        user.uio_iovcnt= 1;
        user.uio_offset= file->of_offset;
        user.uio_resid= size;
        user.uio_segflg= UIO_USERSPACE;
        user.uio_rw= UIO_WRITE;
        user.uio_space= curthread->p_addrspace;

        result= VOP_WRITE(file->of_vnode, & user);
        if(result)
        {
        	return result;
        }

        //*retval= size- user.uio_resid;
       // file-> of_offset= user.uio_offset;

        lock_release(file->of_offsetlock);


        



	return result;
}
/*
 * close() - remove from the file table.
 */
int sys_close(int fd){
struct filetable *ft;
struct openfile *newfile= NULL;
struct openfile **oldfile_ret;


	
if(filetable_okfd(ft, int fd)==0)
{
	return EBADF;
}

if( curthread -> ft_openfiles ==NULL)
{
	return EBADF;
}

filetable_placeat(ft,newfile,fd,
		  &oldfile_ret);

openfile_decref(oldfile_ret);
	/*
	close
-----
sys_close needs to:
   - validate the fd number (use filetable_okfd)
   - use filetable_placeat to replace curproc's file table entry with NULL
   - check if the previous entry in the file table was also NULL
     (this means no such file was open)
   - decref the open file returned by filetable_placeat

   */
}

/* 
* meld () - combine the content of two files word by word into a new file
*/
int meld ( char *pn1, char *pn2, char *pn3){
}
